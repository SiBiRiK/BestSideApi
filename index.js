const express = require('express');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const axios = require('axios');

const app = express();
const gamelogs = 'https://discordapp.com/api/webhooks/1124678846906642442/s7MKZitXLAAesxrWDF_GGVXSHCyPbPuq28PxYXrYI1EObSz-vhjxHIIb9OHJpHHfDuJa';
const scriptlogs = "https://discordapp.com/api/webhooks/1075757132797976607/tU-NnW183Ymz1DsrYqwJtTBt4wwrYgs5AoqfGdfUzPn9saKTbeURK95py66U37J9JuVs";
app.use(bodyParser.json());

app.post('/scripts', async (req, res) => {
  try {
    const payload = req.body;
    await axios.post(scriptlogs, payload);
    res.status(200).send('Success');
  } catch (error) {
    console.error(error);
    res.status(500).send('Error');
  }
});

app.post('/games', async (req, res) => {
  try {
    const payload = req.body;
    await axios.post(gamelogs, payload);
    res.status(200).send('Success');
  } catch (error) {
    console.error(error);
    res.status(500).send('Error');
  }
});

app.get('/delivery', async (req, res) => {
  try {
    const id = "13951200938";
    res.send(id);
      } catch (error) {
    console.error(error);
	}
});

app.get('/userinfo/:userId', (req, res) => {
const userId = req.params.userId;
const api_url = `https://groups.roblox.com/v2/users/${userId}/groups/roles`;
    fetch(api_url)
        .then(response => response.json())
        .then(data => {
            let groups = data["data"];
            let rank = -1;
            for (let group of groups) {
                if (group.group.id === 14732417) {
                    rank = group.role.rank;
                    break;
                }
            }
if (rank == 50) {
res.json({ wl: 'true', admin: 'false' });
} else if (rank > 50) {
res.json({ wl: 'true', admin: 'true' });
} else {
res.json({ wl: 'false', admin: 'false' });
}
        })
        .catch(error => res.json({ success: false, error: error.message }));
});


app.listen(3000, () => {
  console.log('API running on port 3000');
});
